<?php
/*
                                                  ,--,                                              
                              ____  ,-.----.   ,---.'|                                              
  .--.--.      ,---,        ,'  , `.\    /  \  |   | :       ,---,.            ,---,        ,---,.  
 /  /    '. ,`--.' |     ,-+-,.' _ ||   :    \ :   : |     ,'  .' |          .'  .' `\    ,'  .'  \ 
|  :  /`. / |   :  :  ,-+-. ;   , |||   |  .\ :|   ' :   ,---.'   |        ,---.'     \ ,---.' .' | 
;  |  |--`  :   |  ' ,--.'|'   |  ;|.   :  |: |;   ; '   |   |   .'        |   |  .`\  ||   |  |: | 
|  :  ;_    |   :  ||   |  ,', |  ':|   |   \ :'   | |__ :   :  |-,        :   : |  '  |:   :  :  / 
 \  \    `. '   '  ;|   | /  | |  |||   : .   /|   | :.'|:   |  ;/|        |   ' '  ;  ::   |    ;  
  `----.   \|   |  |'   | :  | :  |,;   | |`-' '   :    ;|   :   .'        '   | ;  .  ||   :     \ 
  __ \  \  |'   :  ;;   . |  ; |--' |   | ;    |   |  ./ |   |  |-,        |   | :  |  '|   |   . | 
 /  /`--'  /|   |  '|   : |  | ,    :   ' |    ;   : ;   '   :  ;/|        '   : | /  ; '   :  '; | 
'--'.     / '   :  ||   : '  |/     :   : :    |   ,/    |   |    \        |   | '` ,/  |   |  | ;  
  `--'---'  ;   |.' ;   | |`-'      |   | :    '---'     |   :   .'        ;   :  .'    |   :   /   
            '---'   |   ;/          `---'.|              |   | ,'          |   ,.'      |   | ,'    
                    '---'             `---`              `----'            '---'        `----'      
                                                                                                    
*/

include("bdd.php");
$bdd = new bdd();

/*$bdd->nouvelle_bdd("blog");*/
$bdd->utiliser_bdd("blog");

/*$bdd->creer_index("articles");
$t = uniqid();
$t2 = uniqid()."2";
$bdd->nouvelle_table("articles");
$bdd->inserer_infos("articles", $t, array(
	'titre' => 'Un système de gestion de bdd par fichier '.$t,
	'contenu' => 'Voici une nouvelle aventure, celle de création d\' une alternative aux sgbd propriétaires.',
	'date'	=> time()
	));
$bdd->inserer_infos("articles", $t2, array(
	'titre' => 'Un second titre '.$t2,
	'contenu' => 'Le contenu 2',
	'date'	=> time()
	));
$bdd->ajout_index("articles", $t);
$bdd->ajout_index("articles", $t2);
$bdd->suppression_index("articles", "56477738e9dc0");*/
/*echo "<pre>";
print_r($bdd->ressortir_infos_par_index("articles", "titre", "asc", "0,3"));
echo "</pre>";*/
?>


<h2>La liste des articles:</h2>
<br /><br />

<ul>
	<?php
		foreach ($bdd->ressortir_index('articles', 'asc', '0,10') as $key => $value) {
			echo "<li>".$bdd->ressortir_infos('articles', $value)['date']."</li>";
		}
	?>
</ul>