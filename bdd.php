<?php
Class bdd {
	public $bdd_en_cours;
	public function __construct() {
		/*if(isset($_GET['prompt']))
		{
			$this->prompt();
			exit();
		}*/
	}
	public function nouvelle_bdd($nom) {
		if(!is_dir("stockage/".$nom))
		{
			mkdir("stockage/".$nom);
			return True;
		}
		else
		{
			return False;
		}
	}
	public function utiliser_bdd($nom) {
		$this->bdd_en_cours = $nom;
	}
	public function nouvelle_table($nom) {
		if(!is_dir("stockage/".$this->bdd_en_cours."/".$nom))
		{
			mkdir("stockage/".$this->bdd_en_cours."/".$nom);
			return True;
		}
		else
		{
			return False;
		}
	}
	public function ressortir_infos($endroit, $identifiant) {
		$identifiant = str_split($identifiant, 1);
		$sauvegarde = '';
		foreach ($identifiant as $key => $value) {
			$sauvegarde .= "/".$value;
		}
		if(file_exists("stockage/".$this->bdd_en_cours."/".$endroit."/".$sauvegarde."/fichier"))
		{
			$donnees = array();
			$fp = fopen("stockage/".$this->bdd_en_cours."/".$endroit."/".$sauvegarde."/fichier", "r");
			if($fp)
			{
				while(!feof($fp))
				{
					$buffer = fgets($fp);
					if(preg_match(" [%<==>%] ", $buffer))
					{
						$donnees[explode(' [%<==>%] ', $buffer)[0]] = explode(' [%<==>%] ', $buffer)[1];
					}
				}
				
				fclose($fp);
			}
			return $donnees;
		}
		else
		{
			return False;
		}
	}
	public function inserer_infos($endroit, $identifiant, $valeurs, $modification = False) {
		$identifiant = str_split($identifiant, 1);
		$sauvegarde = '';
		foreach ($identifiant as $key => $value) {
			$sauvegarde .= "/".$value;
			if(!is_dir("stockage/".$this->bdd_en_cours."/".$endroit."/".$sauvegarde."/"))
			{
				mkdir("stockage/".$this->bdd_en_cours."/".$endroit."/".$sauvegarde."/");
			}
		}
		if(file_exists("stockage/".$this->bdd_en_cours."/".$endroit."/".$sauvegarde."/fichier") AND $modification != True)
		{
			return False;
		}
		else
		{
			if($modification == True)
			{
				unlink("stockage/".$this->bdd_en_cours."/".$endroit."/".$sauvegarde."/fichier");
			}
			$fp = fopen("stockage/".$this->bdd_en_cours."/".$endroit."/".$sauvegarde."/fichier", "a+");
			foreach ($valeurs as $key => $value) {
				fputs($fp, $key." [%<==>%] ".$value."\n");
			}
			fclose($fp);
			return True;
		}
	}
	public function creer_index($nom) {
		if(!file_exists("stockage/".$this->bdd_en_cours."/index.".$nom))
		{
			$fp = fopen("stockage/".$this->bdd_en_cours."/index.".$nom, "a+");
			
			fclose($fp);
			return True;
		}
		else
		{
			return False;
		}
	}
	public function ajout_index($nom, $valeur) {
		if(file_exists("stockage/".$this->bdd_en_cours."/index.".$nom))
		{
			$fp = fopen("stockage/".$this->bdd_en_cours."/index.".$nom, "a+");
			fputs($fp, $nom."::".$valeur."\n");	
			fclose($fp);
			return True;
		}
		else
		{
			return False;
		}
	}
	public function supprimer_index($nom) {
		if(file_exists("stockage/".$this->bdd_en_cours."/index.".$nom))
		{
			unlink("stockage/".$this->bdd_en_cours."/index.".$nom);
			return True;
		}
		else
		{
			return False;
		}
	}
	public function suppression_index($nom, $identifiant)
	{
		$ok = False;
		if(file_exists("stockage/".$this->bdd_en_cours."/index.".$nom))
		{
			/*$fp = fopen("stockage/".$this->bdd_en_cours."/index.".$nom, "a+");
			if($fp)
			{
				while (!feof($fp))
				{
					echo feof($fp);
					if(preg_match("#".$identifiant."#", fgets($fp)))
					{
						$buffer
						$ok = True;
					}
				}
			}*/
			file_put_contents("stockage/".$this->bdd_en_cours."/index.".$nom, str_replace($nom."::".$identifiant."\n", "", file_get_contents("stockage/".$this->bdd_en_cours."/index.".$nom)));
		}
		if($ok)
		{
			return True;
		}
		else
		{
			return False;
		}
	}
	public function ressortir_infos_par_index($nom_index, $tri = '', $ordre = '', $limite = '0,25') {
		if(file_exists("stockage/".$this->bdd_en_cours."/index.".$nom_index))
		{
			$fp = fopen("stockage/".$this->bdd_en_cours."/index.".$nom_index, "r");
			if($fp)
			{
				$donnees = array();
				$i = 0;
				while (!feof($fp) AND $i < explode(',',$limite)[1] AND $i >= explode(',',$limite)[0])
				{
						$buffer = fgets($fp);
						if(preg_match("#::#", $buffer))
						{
							$buffer = explode("::", $buffer);

							$buffer_split_ = str_split($buffer[1], 1);
							$buffer_split = "";

							foreach($buffer_split_ as $key => $value)
							{
								$buffer_split .= "/".$value;
							}
							
							$buffer_split = rtrim($buffer_split);

							//echo $buffer_split."<br />";

							if(file_exists("stockage/".$this->bdd_en_cours."/".$buffer[0].$buffer_split."fichier"))
							{
								$fp2 = fopen("stockage/".$this->bdd_en_cours."/".$buffer[0].$buffer_split."fichier", "r");
								if($fp2)
								{
									$donnees[$i] = array();
									while(!feof($fp2))
									{
										$buffer__ = fgets($fp2);
										if(preg_match(" [%<==>%] ", $buffer__))
										{

											$donnees[$i][explode(' [%<==>%] ', $buffer__)[0]] = explode(' [%<==>%] ', $buffer__)[1];
											
										}
									}
									if($tri != '')
									{
										$donnees[$donnees[$i][$tri]] = $donnees[$i];
										unset($donnees[$i]);
									}
								}
								fclose($fp2);
							}
						}
					$i++;
				}
				fclose($fp);
				if($ordre == 'asc')
				{
					ksort($donnees);
				}
				if($ordre == 'desc')
				{
					krsort($donnees);
				}
				if($ordre == 'sort')
				{
					sort($donnees);
				}
				if($ordre == 'alea')
				{
					shuffle($donnees);
				}
				return $donnees;
			}
		}
		else
		{
			return False;
		}
	}
	public function ressortir_index($nom_index, $ordre = '', $limite = '0,25') {
		if(file_exists("stockage/".$this->bdd_en_cours."/index.".$nom_index))
		{
			$fp = fopen("stockage/".$this->bdd_en_cours."/index.".$nom_index, "r");
			if($fp)
			{
				$donnees = array();
				$i = 0;

				while (!feof($fp) AND $i < explode(',',$limite)[1])
				{
						$buffer = fgets($fp);
						if(preg_match("#::#", $buffer))
						{
							$buffer = explode("::", $buffer);
							
							$buffer = rtrim($buffer[1]);

							$donnees[] = $buffer;
						}
					$i++;
				}
				fclose($fp);
				if($ordre == 'asc')
				{
					ksort($donnees);
				}
				if($ordre == 'desc')
				{
					krsort($donnees);
				}
				if($ordre == 'sort')
				{
					sort($donnees);
				}
				if($ordre == 'alea')
				{
					shuffle($donnees);
				}
				return $donnees;
			}
		}
		else
		{
			return False;
		}
	}
	public function prompt() {
		echo "<h1>Simple DB -- Prompt</h1>";
		echo "<form method='POST'>";
			echo "<textarea name='command' rows='5' cols='200' style='background-color: #000000; color: white;'></textarea>";
			echo "<br /><input type='submit' name='exec' style='background-color: grey; color: white; height: 40px; width: 100px;'>";
		echo "</form>";
		if(isset($_POST['command']))
		{
			$_POST['command'] = explode(';', $_POST['command']);
			print_r($_POST['command']);
			foreach($_POST['command'] as $key => $value)
			{
				if(preg_match("#nouvelle bdd#", $value))
				{
					$command = str_replace('nouvelle bdd ', '', $value);
					if($this->nouvelle_bdd($command))
					{
						echo "BDD `".$command."` cree";
					}
				}
				if(preg_match("#nouvelle table#", $value))
				{
					$command = str_replace('nouvelle table ', '', $value);
					$commands = explode('.', $command);
					if($this->utiliser_bdd($commands[0]))
					{
						echo "BDD ".$commands[0]." choisie<br />";
					}
					if($this->nouvelle_table($commands[1]))
					{
						echo "TABLE ".$commands[1]." cree";
					}
				}
			}
		}
	}
}