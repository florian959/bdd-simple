<?php
/* AJOUT DE LA LIBRAIRIE */
include("bdd.php");
/* UTILISATION DE LA LIBRAIRIE */
$bdd = new bdd();

/* CREATION D'UNE BASE DE DONNEES */
$bdd->nouvelle_bdd("membres");

/* SELECTION DE LA BASE DE DONNEES */
$bdd->utiliser_bdd("membres");

/* CREATION D'UNE TABLE A L'INTERIEUR */
$bdd->nouvelle_table("comptes");

/* INSERTION DE DONNEES A L'INTERIEUR */
if(!$bdd->inserer_infos("comptes", "florian2", array('nom_utilisateur ' => 'florian', 'prenom' => 'Florian')))
{
	echo "Cette entree existe deja<br />";
}

/* MODIFICATION DES DONNEES A L'INTERIEUR */
$test = $bdd->ressortir_infos("comptes", "florian2");
$test['prenom'] = "Florian Hourdin :D";

/* MISE A JOUR DE LA BASE DE DONNEES */
if($bdd->inserer_infos("comptes", "florian2", $test, True))
{
	echo "Succes de la mise a jour. <br />";
}
else
{
	echo "Echec de la mise a jour. <br />";
}

/* AFFICHER DES DONNEES */
if(!$bdd->ressortir_infos("comptes", "florian2"))
{
	echo "Cette entree n'existe pas<br />";
}
else
{
	echo $bdd->ressortir_infos("comptes", "florian2")['prenom']."<br />";
}

/* CREER UN INDEX */
$bdd->nouvelle_table("articles");
$bdd->creer_index("articles");

/* AJOUTER A UN INDEX */
$bdd->inserer_infos("articles", "identifiant", array(
	'titre' => 'Un second titre',
	'contenu' => 'Le contenu',
	'date'	=> time()
	));

$bdd->ajout_index("articles", "identifiant");

/* AFFICHAGE A PARTIR D'UN INDEX */
/* index articles, triés par date de manière ascendante */
print_r($bdd->ressortir_infos_par_index("articles", "date", "asc"));
?>